//
//  ViewController.swift
//  LayoutAnchors
//
//  Created by JOSEPH KERR on 8/15/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit


/**
 View controller to present buttons with layout guides between them
 
 A variable topLayoutConstraint is modified for spacinf from the top
 
 Two variables spacingConstraint1 and spacingConstraint2 is used for spacing between
 buttons
 
 Usage:
 
 - Pressing the action button will alter constraints and animate the changes
 - Pressing the action button again will alter again
 - Pressing the action again will alter the constraints so that the buttons are pinned to the
 bottom
 
 Notes:
 Changing Either save or cancel button title that and it becomes the larger title will 
 effect size of the other as the widthAnchors are equal
 */

class ViewController: UIViewController {
    
    var mode = 0
    
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    let contextButton: UIButton = UIButton(type: .system)
    
    var layoutSpace1: UILayoutGuide?
    var layoutSpace2: UILayoutGuide?
    var layoutSpace3: UILayoutGuide?

    var topLayoutConstraint: NSLayoutConstraint?
    var spacingConstraint1: NSLayoutConstraint?
    var spacingConstraint2: NSLayoutConstraint?

    var equalSpacing = true
    var origColor: UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        origColor = self.view.backgroundColor
        
        if let navC = self.navigationController {
            contextButton.setTitle(navC.isToolbarHidden ?  "Show Toolbar" : "Hide Toolbar" , for: UIControlState())
        } else {
            contextButton.setTitle("contextButton", for: UIControlState())
        }
        contextButton.backgroundColor = UIColor.lightGray
        contextButton.translatesAutoresizingMaskIntoConstraints = false
        contextButton.frame = CGRect(x:0, y:0,width: 80,height: 40)
        contextButton.addTarget(self, action: #selector(someAction(_:)), for: .touchUpInside)
        view.addSubview(contextButton)

        cancelButton.setTitle(" Cancel Button ", for: UIControlState())
        saveButton.setTitle(" Save ", for: UIControlState())
        
        setupLayoutGuidesAndStartPositions()
        
        mode = 1
        
        actionButton.setTitle("Action Mode \(mode)", for: UIControlState())

    }
    
    
    
    func setupLayoutGuidesAndStartPositions() {
        let space1 = UILayoutGuide()
        let space2 = UILayoutGuide()
        let space3 = UILayoutGuide()
        
        view.addLayoutGuide(space1)
        view.addLayoutGuide(space2)
        view.addLayoutGuide(space3)
        
        let margins = view.layoutMarginsGuide
        
        /*
         
         edge-|[ space ][SaveButton][ space ][CancelButton][ space ]|-edge
         
         */
        
        // Leading Space
        margins.leadingAnchor.constraint(equalTo: space1.leadingAnchor).isActive = true
        space1.trailingAnchor.constraint(equalTo: saveButton.leadingAnchor).isActive = true
        // Middle Space
        saveButton.trailingAnchor.constraint(equalTo: space2.leadingAnchor).isActive = true
        space2.trailingAnchor.constraint(equalTo: cancelButton.leadingAnchor).isActive = true
        // Trailing Space
        cancelButton.trailingAnchor.constraint(equalTo: space3.leadingAnchor).isActive = true
        space3.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        
        saveButton.widthAnchor.constraint(equalTo: cancelButton.widthAnchor).isActive = true
        
        // Equal spacing between or not
        if equalSpacing {
            spacingConstraint1 = space1.widthAnchor.constraint(equalTo: space2.widthAnchor)
            spacingConstraint2 = space1.widthAnchor.constraint(equalTo: space3.widthAnchor)
        } else {
            // Leading and trailing are equal  space1 = space3
            // Middle space2 is percentage of space1
            spacingConstraint1 = space1.widthAnchor.constraint(equalTo: space3.widthAnchor)
            spacingConstraint2 = space2.widthAnchor.constraint(equalTo: space1.widthAnchor, multiplier: 0.65)
        }
        spacingConstraint1?.isActive = true
        spacingConstraint2?.isActive = true
        
        // Set the topAnchor of space1
        topLayoutConstraint = space1.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 120.0)
        topLayoutConstraint?.isActive = true
        // All other topAnchor spacing is based on space1
        space2.topAnchor.constraint(equalTo: space1.topAnchor).isActive = true
        space3.topAnchor.constraint(equalTo: space1.topAnchor).isActive = true
        saveButton.topAnchor.constraint(equalTo: space1.topAnchor).isActive = true
        cancelButton.topAnchor.constraint(equalTo: space1.topAnchor).isActive = true
        
        // Keep a reference to the spaces
        layoutSpace1 = space1
        layoutSpace2 = space2
        layoutSpace3 = space3
        
        // Setup the action button
        actionButton.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        actionButton.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        actionButton.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 30.0).isActive = true
        
        // Setup the some button
        contextButton.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        contextButton.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        contextButton.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 80.0).isActive = true
        
    }
    
    
    @IBAction func onAction(_ sender: UIButton) {

        guard let space1 = layoutSpace1,
            let space2 = layoutSpace2,
            let space3 = layoutSpace3
            else{
                return
        }

        var bgColor = UIColor.white
        
        if mode == 0 {

            // Start up

            if let oColor = origColor {
                bgColor = oColor
            }
            topLayoutConstraint?.isActive = false
            topLayoutConstraint = space1.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 120.0)
            topLayoutConstraint?.isActive = true

            
            spacingConstraint1?.isActive = false
            spacingConstraint2?.isActive = false

            if equalSpacing {
                spacingConstraint1 = space1.widthAnchor.constraint(equalTo: space2.widthAnchor)
                spacingConstraint2 = space1.widthAnchor.constraint(equalTo: space3.widthAnchor)
            } else {
                spacingConstraint1 = space1.widthAnchor.constraint(equalTo: space3.widthAnchor)
                spacingConstraint2 = space2.widthAnchor.constraint(equalTo: space1.widthAnchor, multiplier: 0.65)
            }
            
            spacingConstraint1?.isActive = true
            spacingConstraint2?.isActive = true
            
        } else if mode == 1 {

            if let oColor = origColor {
                bgColor = oColor
            }

            topLayoutConstraint?.isActive = false
            topLayoutConstraint = space1.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 170.0)
            topLayoutConstraint?.isActive = true
            
            
            spacingConstraint1?.isActive = false
            spacingConstraint2?.isActive = false

            spacingConstraint1 = space1.widthAnchor.constraint(equalTo: space3.widthAnchor)
            spacingConstraint2 = space2.widthAnchor.constraint(equalTo: space1.widthAnchor, multiplier: 0.45)
            
            spacingConstraint1?.isActive = true
            spacingConstraint2?.isActive = true

        } else if mode == 2 {

            if let oColor = origColor {
                bgColor = oColor
            }
            topLayoutConstraint?.isActive = false
            topLayoutConstraint = space1.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 200.0)
            topLayoutConstraint?.isActive = true
            
            
            spacingConstraint1?.isActive = false
            spacingConstraint2?.isActive = false

            spacingConstraint1 = space1.widthAnchor.constraint(equalTo: space3.widthAnchor)
            spacingConstraint2 = space2.widthAnchor.constraint(equalTo: space1.widthAnchor, multiplier: 0.15)
            
            spacingConstraint1?.isActive = true
            spacingConstraint2?.isActive = true

        } else if mode == 3 {
            
            bgColor = UIColor.black
            topLayoutConstraint?.isActive = false
            let spacing = saveButton.frame.height + 8  // from bottom
            topLayoutConstraint = space1.topAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: -spacing)
            topLayoutConstraint?.isActive = true
            
        } else {
            bgColor = UIColor.white
        }

    
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
            self.view.layoutIfNeeded()
            self.view.backgroundColor = bgColor
            }, completion: nil)

        mode += 1
        
        if mode > 3 {
            mode = 0
        }
        
        sender.setTitle("Action Mode \(mode)", for: UIControlState())
        
    }

    
    func someAction(_ sender: UIButton) {
        
        if let navC = self.navigationController {
            contextButton.setTitle(navC.isToolbarHidden ? "Hide Toolbar" : "Show Toolbar" , for: UIControlState())
            
            navC.setToolbarHidden(!navC.isToolbarHidden, animated: true)
        }
    }


}


//        space1.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        space2.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        space3.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        saveButton.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        cancelButton.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true


